package com.epam.microservices.ProductService;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.epam.microservices.productservice.controller.ProductServiceController;
import com.epam.microservices.productservice.model.Product;
import com.epam.microservices.productservice.service.ProductService;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductServiceController.class)
public class ProductServiceControllerTest {

	@Autowired
	private MockMvc mockmvc;

	@MockBean
	private ProductService productService;

	static Product mockProd1 = new Product(1, "prod 1", "prod desc1", BigDecimal.valueOf(789.00));
	static Product mockProd2 = new Product(2, "prod 2", "prod desc2", BigDecimal.valueOf(123.00));
	static Product mockProd3 = new Product(3, "prod 3", "prod desc3", BigDecimal.valueOf(456));
	static List<Product> prodList = new ArrayList<>();
	static JSONObject mockProductJson;

	@BeforeClass
	public static void initialize() throws JSONException {

		prodList.add(mockProd1);

		mockProductJson = new JSONObject();

		mockProductJson.accumulate("id", 1);
		mockProductJson.accumulate("name", "prod 1");
		mockProductJson.accumulate("description", "prod desc1");
		mockProductJson.accumulate("unitPrice", BigDecimal.valueOf(789.00));

	}

	@Test
	public void getProducts() throws Exception {

		Mockito.when(productService.getProducts()).thenReturn(prodList);

		JSONArray mockResponse = new JSONArray();
		mockResponse.put(mockProductJson);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/products").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockmvc.perform(requestBuilder).andReturn();

		JSONAssert.assertEquals(mockResponse.toString(), result.getResponse().getContentAsString(), false);
	}

	@Test
	public void getProductById() throws Exception {

		Mockito.when(productService.getProductById((1L))).thenReturn(prodList.stream().findFirst());

		JSONArray mockResponse = new JSONArray();
		mockResponse.put(mockProductJson);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/products/1").accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockmvc.perform(requestBuilder).andReturn();

		JSONAssert.assertEquals(mockResponse.get(0).toString(), result.getResponse().getContentAsString(), true);
	}

	@Test
	public void saveProducts() throws Exception {

		Mockito.when(productService.saveProduct(Mockito.any())).thenReturn(mockProd2);

		JSONObject mockResponse = new JSONObject();
		mockResponse.accumulate("id", 2);
		mockResponse.accumulate("name", "prod 2");
		mockResponse.accumulate("description", "prod desc2");
		mockResponse.accumulate("unitPrice", BigDecimal.valueOf(123.00));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/products").content(mockResponse.toString())
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockmvc.perform(requestBuilder).andReturn();

		JSONAssert.assertEquals(mockResponse, new JSONObject(result.getResponse().getContentAsString()), true);
	}

	@Test
	public void updateProducts() throws Exception {

		Mockito.when(productService.updateProduct(2, mockProd2)).thenReturn(mockProd2);

		JSONObject mockResponse = new JSONObject();
		mockResponse.accumulate("id", 2L);
		mockResponse.accumulate("name", "prod 2");
		mockResponse.accumulate("description", "prod desc2");
		mockResponse.accumulate("unitPrice", BigDecimal.valueOf(123.00));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put("/products/2").content(mockResponse.toString())
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockmvc.perform(requestBuilder).andReturn();
		String content = result.getResponse().getContentAsString();
		JSONAssert.assertEquals(mockResponse, new JSONObject(content), JSONCompareMode.LENIENT);
	}

	@Test
	public void deleteProduct() throws Exception {

		Mockito.when(productService.deleteProductById(3)).thenReturn(false);

		JSONObject mockResponse = new JSONObject();
		mockResponse.accumulate("id", 3);
		mockResponse.accumulate("name", "prod 3");
		mockResponse.accumulate("description", "prod desc3");
		mockResponse.accumulate("unitPrice", BigDecimal.valueOf(456));
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/products/3").content(mockResponse.toString())
				.accept(MediaType.APPLICATION_JSON);

		MvcResult result = mockmvc.perform(requestBuilder).andReturn();

		assertEquals(false, false);
	}

}
