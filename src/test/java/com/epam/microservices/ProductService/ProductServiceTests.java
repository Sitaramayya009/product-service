package com.epam.microservices.ProductService;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.epam.microservices.productservice.exceptions.ProductNotFoundException;
import com.epam.microservices.productservice.model.Product;
import com.epam.microservices.productservice.repository.ProductRepository;
import com.epam.microservices.productservice.service.ProductServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {

	@Mock
	private ProductRepository productRepository;

	@InjectMocks
	private ProductServiceImpl productService;

	Product product1 = new Product(1, "BMW-C", "Toy Car", BigDecimal.valueOf((750)));
	Product product2 = new Product(2, "BENZ", "SUV", BigDecimal.valueOf((1350)));

	@Test
	public void getProducts() {
		List<Product> productList = new ArrayList<>();
		productList.add(product2);
		productList.add(product1);

		productList.stream().filter(prod -> prod.getName().equals("BMW")).collect(Collectors.toList());
		when(productRepository.findAll()).thenReturn(productList);
		List<Product> prodList = productService.getProducts();
		assertEquals(productList, prodList);

	}

	@Test
	public void getProductById() {

		when(productRepository.findById(anyLong())).thenReturn(Optional.of(product1));
		Optional<Product> prod = productService.getProductById(1L);
		assertEquals(product1, prod.get());
	}

	@Test(expected = ProductNotFoundException.class)
	public void testProductNotFoundException() {

		when(productRepository.findById(anyLong())).thenReturn(Optional.empty());
		Optional<Product> prod = productService.getProductById(1L);
		prod.orElseThrow(() -> new ProductNotFoundException("id-" + 1L));

	}

	@Test(expected = NoSuchElementException.class)
	public void testNoSuchElementException() {

		when(productRepository.findById(anyLong())).thenReturn(Optional.empty());
		Optional<Product> prod = productService.getProductById(1L);
		prod.get();
	}

	@Test
	public void testSaveProduct() {

		when(productRepository.save(product1)).thenReturn(product1);
		Product prod = productService.saveProduct(product1);
		assertEquals(prod, product1);
	}

	@Test
	public void testsaveOrupdateProduct() {

		when(productRepository.save(product1)).thenReturn(product1);
		Product updateProd = productService.updateProduct(1L, product1);
		assertEquals(updateProd, product1);
	}

	@Test
	public void testingMethod() {
		String message = "Test";
		assertEquals(3, message.length());
	}
}
