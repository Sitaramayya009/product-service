package com.epam.microservices.productservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.epam.microservices.productservice.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
