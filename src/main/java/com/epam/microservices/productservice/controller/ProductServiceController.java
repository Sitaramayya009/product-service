package com.epam.microservices.productservice.controller;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.net.URI;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.epam.microservices.productservice.ProductReviewServiceProxy;
import com.epam.microservices.productservice.exceptions.ProductNotFoundException;
import com.epam.microservices.productservice.model.Product;
import com.epam.microservices.productservice.model.ProductReview;
import com.epam.microservices.productservice.service.ProductReviewService;
import com.epam.microservices.productservice.service.ProductService;

@RestController
public class ProductServiceController {

	private static Logger logger = LoggerFactory.getLogger(ProductServiceController.class);

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductReviewService productReviewService;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	private ProductReviewServiceProxy proxy;

	@GetMapping("/products")
	public ResponseEntity<List<Product>> getProducts() {
		return ResponseEntity.ok(productService.getProducts());
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/products/feign/{id}")
	public ResponseEntity<Resource<Product>> getProductByIdFeign(@PathVariable long id) {

		return productService.getProductById(id).map(product -> {
			product.setReviews(proxy.getProductReviews("SharedSecret", id));
			Resource<Product> resource = new Resource<Product>(product);
			ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).getProducts());
			resource.add(linkTo.withRel("all-products"));
			return ResponseEntity.ok(resource);
		}).orElseThrow(() -> new ProductNotFoundException("id-" + id));

	}

	@GetMapping("/products/{id}")
	public ResponseEntity<Resource<Product>> getProductById(@PathVariable long id) {

		return productService.getProductById(id).map(product -> {
			product.setReviews(productReviewService.getProductReviews(id));
			Resource<Product> resource = new Resource<Product>(product);
			ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).getProducts());
			resource.add(linkTo.withRel("all-products"));
			return ResponseEntity.ok(resource);
		}).orElseThrow(() -> new ProductNotFoundException("id-" + id));

	}

	/**
	 * 
	 * @param product
	 * @return
	 */
	@PostMapping("/products")
	public ResponseEntity<Product> saveProducts(@RequestBody Product product) {
		productService.saveProduct(product);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(product.getId())
				.toUri();
		return ResponseEntity.created(location).body(product);
	}

	/**
	 * 
	 * @param id
	 * @param product
	 * @return
	 */
	@PutMapping("/products/{id}")
	public ResponseEntity<Product> saveOrupdateProductById(@PathVariable int id, @RequestBody Product product) {
		return new ResponseEntity<Product>(productService.updateProduct(id, product), HttpStatus.OK);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	@DeleteMapping("/products/{id}")
	public ResponseEntity<?> deleteProductById(@PathVariable long id) {
		boolean isDeleted = productService.deleteProductById(id);
		if (!isDeleted)
			throw new ProductNotFoundException("id-" + id);
		return ResponseEntity.noContent().build();
	}

	/**
	 * To add a review against the product
	 * 
	 * @param review
	 * @param id
	 * @return
	 */
	@PostMapping("/products/{id}/reviews")
	public ResponseEntity<ProductReview> saveReviews(@RequestBody ProductReview review, @PathVariable int id) {
		productReviewService.saveProductReview(id, review);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(review.getId())
				.toUri();
		return ResponseEntity.created(location).body(review);
	}

}
