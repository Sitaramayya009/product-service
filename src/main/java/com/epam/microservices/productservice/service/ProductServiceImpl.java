package com.epam.microservices.productservice.service;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.microservices.productservice.model.Product;
import com.epam.microservices.productservice.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	private static Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	private ProductRepository productRepo;

	/**
	 * get all product details
	 */
	@Override
	public List<Product> getProducts() {

		return productRepo.findAll();
	}

	/**
	 * Saving product details
	 */
	@Override
	public Product saveProduct(Product product) {
		productRepo.save(product);
		return product;
	}

	/**
	 * Getting selected product details using id
	 */
	@Override
	public Optional<Product> getProductById(long id) {

		return productRepo.findById(id);
	}

	/**
	 * Updating product details
	 */
	@Override
	public Product updateProduct(long id, Product product) {
		Optional<Product> updateProd = productRepo.findById(id);
		if (updateProd.isPresent()) {
			updateProd.get().setName(product.getName());
			updateProd.get().setDescription(product.getDescription());
			updateProd.get().setUnitPrice(product.getUnitPrice());
			productRepo.save(updateProd.get());
		}
		return product;
	}

	/**
	 * Deleting product details.
	 */
	@Override
	public boolean deleteProductById(long id) {
		return productRepo.findById(id).map(prod -> {
			productRepo.deleteById(id);
			return true;
		}).orElse(false);

	}

}
