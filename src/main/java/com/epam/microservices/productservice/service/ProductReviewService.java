package com.epam.microservices.productservice.service;

import java.util.List;

import com.epam.microservices.productservice.model.ProductReview;

public interface ProductReviewService {

	public List<ProductReview> getProductReviews(Long id);

	public ProductReview saveProductReview(long id, ProductReview review);

}
